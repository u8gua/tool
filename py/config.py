class CONFIG:

    DNSPOD = "34465", "8117ba3589b3a8c02acb3d4a2fb1a478"

    class HOST:
        LOCALHOST = "8gua.win"
        TXT = "8gua.win"

    GIT = [
        "https://gitee.com/u8gua/8gua-srv.git",
        "https://github.com/8gua-blog/8gua-srv.git",
    ]
